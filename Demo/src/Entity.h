#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

class Entity {
private:
	glm::mat4 translation;
	glm::mat4 scale;
	glm::quat rotation;
public:
	Entity() {
		//
	}

	glm::mat4 getTransformation() {
		return translation * glm::toMat4(rotation) * scale;
	}

	void scaleBy(glm::vec3 s) { scale = glm::scale(scale, s); }
	void setScale(glm::vec3 s) { scale = glm::scale(glm::mat4(1.0), s); }

	void translateBy(glm::vec3 t) { translation = glm::translate(translation, t); }
	void setTranslate(glm::vec3 t) { translation = glm::translate(glm::mat4(1.0), t); }

	void rotateBy(glm::quat r) { rotation = r * rotation; }
	void setRotation(glm::quat r) { rotation = r; }
};