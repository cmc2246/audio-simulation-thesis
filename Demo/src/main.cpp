// This would be the hello world example

#include <iostream>

#include "Renderer.h"

int main() {
	printf("Staring up renderer. Please be patient...\n");

	Renderer rend;
	rend.load_asset("models/vokselia_spawn");

	return 0;
}
