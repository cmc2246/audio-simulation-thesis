#pragma once

#ifdef _WIN32
#include <windows.h>
#include <fcntl.h>
#include <io.h>
#include <ShellScalingAPI.h>
// it might be nice to support other platforms
#endif

// STL
#include <algorithm>
#include <array>
#include <assert.h>
#include <chrono>
#include <ctime>
#include <iostream>
#include <numeric>
#include <random>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <sys/stat.h>
#include <unordered_map>
#include <vector>

// Vulkan
#include <vulkan/vulkan.h>

// GLM
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

// RT Audio
#include "RTAudioDriver.h"

class Renderer {
protected:
	// Audio
	RTAudioDriver audio_driver;
	// General
	std::string window_title = "Audio Demo";
	std::string shaderDir = "shaders";
	unsigned int frameCounter = 0;
	unsigned int lastFPS = 0;
	std::chrono::time_point<std::chrono::high_resolution_clock> lastTimestamp;
	struct Settings {
		bool validation = false;
		bool fullscreen = false;
		bool vsync = false;
		bool overlay = false;
	} settings;
	// rendering
	bool viewUpdated = false;
	unsigned int destWidth;
	unsigned int destHeight;
	bool resizing = false;
	VkClearColorValue defaultClearColor = { { 0.025f, 0.025f, 0.025f, 1.0f } };
#ifdef _WIN32
	HWND window;
	HINSTANCE windowInstance;
#else
	// presumably we could support other platforms
#endif
	// Vulkan
	std::vector<std::string> supportedInstanceExtensions; 
	VkInstance instance;
	VkPhysicalDevice physicalDevice;
	VkPhysicalDeviceProperties physicalDeviceProperties;
	VkPhysicalDeviceFeatures physicalDeviceFeatures;
	VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
	VkPhysicalDeviceFeatures physicalDeviceEnabledFeatures{};
	std::vector<const char*> enabledDeviceExtensions;
	std::vector<const char*> enabledInstanceExtensions;
	VkDevice device;
	VkQueue queue;
	VkFormat depthFormat;
	VkCommandPool cmdPool;
	VkPipelineStageFlags submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	VkSubmitInfo submitInfo;
	std::vector<VkCommandBuffer> drawCmdBuffers;
	VkRenderPass renderPass;
	std::vector<VkFramebuffer>frameBuffers;
	uint32_t currentBuffer = 0;
	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
	std::vector<VkShaderModule> shaderModules;
	VkPipelineCache pipelineCache;
	//VulkanSwapChain swapChain; // todo fix
	struct {
		VkSemaphore presentComplete;
		VkSemaphore renderComplete;
	} semaphores;
	std::vector<VkFence> waitFences;
	bool prepared = false;
	bool resized = false;
	//vks::UIOverlay UIOverlay; // todo fix
	float frameTimer = 1.0f;
	// Deferred 
	int debugDisplayTarget = 0;

public:
	// Construct/Destruct
	Renderer() {
		destHeight = 600;
		destWidth = 800;
		audio_driver.Initialize();
	}
	~Renderer() {
		//
	}

	// Methods
	std::string getWindowTitle() {
		return window_title;
	}

	void windowResize();
	void handleMouseMove(int32_t x, int32_t y);
	void nextFrame();
	void updateOverlay();
	void createPipelineCache();
	void createCommandPool();
	void createSynchronizationPrimitives();
	void initSwapchain();
	void setupSwapChain();
	void createCommandBuffers();
	void destroyCommandBuffers();

	bool load_asset(const char* path) {
		return true;
	}
};

