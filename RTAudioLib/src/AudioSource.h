#pragma once

#include <string>
#include <xaudio2.h>

#include "Resources.h"

using byte = uint8_t; // readability thing

// stores the audio data in memory
class AudioSource {
private:
	std::string path;
	AudioType format_type;
	WAVEFORMATEX format;
	
	byte* data;

public:

};