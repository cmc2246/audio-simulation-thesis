#pragma once

#include <iostream>
#include <vector>

#include <Windows.h>
#include <xaudio2.h>

#include <vulkan/vulkan.h>

class RTAudioDriver {
protected:
	bool initialized = false;
	// Xaudio2 resources
	IXAudio2* pXAudio2 = nullptr;
	IXAudio2MasteringVoice* pMasterVoice = nullptr;
	IXAudio2SourceVoice* pSourceVoice = nullptr;
	std::vector<int> activeChannels;
	std::vector<int> idleChannels;
	// vulkan info
	VkInstance* instance = nullptr;
	// audio codec stuff
	// geometry info
	VkAccelerationStructureKHR* geometry = nullptr;

public:
	RTAudioDriver() {
		initialized = false;
	}

	~RTAudioDriver() {
		delete pMasterVoice;
		delete pSourceVoice;
		delete pXAudio2;
	}

	void updateAccelerationStructure(VkAccelerationStructureKHR* data) { geometry = data; }

	HRESULT addVoice() {
		return S_OK;
	}

	HRESULT addSubmix() {
		return S_OK;
	}

	/// <summary>
	/// Connects a non-terminating audio stream to a voice.
	/// </summary>
	/// <returns></returns>
	HRESULT playStream() {
		return S_OK;
	}

	/// <summary>
	/// Plays a terminating audio segment to a voice
	/// </summary>
	HRESULT playSound() {
		if (!initialized) {
			return E_UNEXPECTED;
		}

		XAUDIO2_BUFFER buffer = { 0 };
		//buffer.pAudioData = (BYTE*)waveInfo.waveData;
		buffer.Flags = XAUDIO2_END_OF_STREAM;
		//buffer.AudioBytes = waveInfo.waveDataSize;
		buffer.LoopCount = XAUDIO2_LOOP_INFINITE;
		//pSourceVoice->SubmitSourceBuffer(&buffer);

		WAVEFORMATEX data = { 0 };


		const wchar_t* strFileName = TEXT("media\\MusicMono.wav");
		// Open the file
		HANDLE hFile = CreateFile(
			strFileName,
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);

		if( INVALID_HANDLE_VALUE == hFile )
			return HRESULT_FROM_WIN32(GetLastError());

		if( INVALID_SET_FILE_POINTER == SetFilePointer(hFile, 0, NULL, FILE_BEGIN) )
			return HRESULT_FROM_WIN32(GetLastError());

		return S_OK;
	}

	/// <summary>
	/// Prepares the audio system to start sending data to the default audio device.
	/// We separate this from the constructor so the HRESULT return code is
	/// </summary>
	HRESULT Initialize() {
		if( initialized )
			return S_OK;

		// I have no idea what this actually does.
		HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
		if( FAILED(hr) )
			return hr;

		// create the XAudio2 framework
		hr = XAudio2Create(&pXAudio2, 0, XAUDIO2_DEFAULT_PROCESSOR);
		if( FAILED(hr) )
			return hr;

		// create the mastering voice, every speaker configuration needs exactly 1 mastering voice
		// why isn't this done in XAudio2Create?!?
		hr = pXAudio2->CreateMasteringVoice(&pMasterVoice);
		if( FAILED(hr) )
			return hr;

		// todo: fix me pls
		//hr = pXAudio2->CreateSourceVoice(&pSourceVoice);


		// Done for now.
		initialized = true;
		return hr;
	}

	HRESULT getDefaultAudioDevice() {
		return S_OK;
	}
};
